﻿using Enums;
using EventMessages;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class Cell : CellBase
{
    public Image Higlight;
    public Text MarkText;

    private Button _button;

    private Color _defaultColor;
    private static readonly Color XColor = new Color(0, 1, 0.4f, 0.35f);
    private static readonly Color OColor = new Color(1, 0.15f, 0, 0.35f);
    private static readonly Color DrawColor = new Color(1, 0.7f, 0, 0.35f);

    protected override void Awake()
    {
        base.Awake();

        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);

        _defaultColor = Higlight.color;
    }

    public void Clear()
    {
        //CellValue = CellValue.Unset;
        SetMarkText();
        UnHighlight();
    }

    private void SetMarkText()
    {
        MarkText.text = CellValue == CellValue.Unset ? string.Empty : CellValue.ToString();
    }

    private void OnClick()
    {
        if (GameManager.GameMode == GameMode.TwoPlayers || GameManager.Turn.Player == PlayerType.Player)
        {
            if (!Higlight.gameObject.activeInHierarchy || Higlight.fillCenter)
            {
                return;
            }
        }
        else
        {
            // Ai player's turn
            return;
        }
        SetValue(GameManager.Turn.Mark);
    }

    public void SetValue(CellValue value, bool silent = false)
    {
        Table.SetCell(Index, value);
        SetMarkText();
        UnHighlight();

        if (!silent)
        {
            EventAggregator.EventAggregator.Instance.Publish(new CellSelectedMessage(this));
        }
    }

    public void Highlight(CellValue mark, bool permanent = false)
    {
        if (!permanent && (CellValue != CellValue.Unset || Higlight.fillCenter))
        {
            return;
        }
        switch (mark)
        {
            case CellValue.X:
                Higlight.color = XColor;
                break;
            case CellValue.O:
                Higlight.color = OColor;
                break;
            case CellValue.Unset:
                Higlight.color = permanent ? DrawColor : _defaultColor;
                break;
        }
        Higlight.fillCenter = permanent;
        Higlight.gameObject.SetActive(true);
    }

    public void UnHighlight()
    {
        Higlight.fillCenter = false;
        Higlight.color = _defaultColor;
        Higlight.gameObject.SetActive(false);
    }
}