﻿using System.Collections.Generic;
using Enums;
using EventAggregator;
using EventMessages;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameManager : UIBehaviour, IHandle<CellSelectedMessage>, IHandle<GameOverMessage>
{
    public Dropdown GameModeDropdown;
    public Dropdown StartPlayerDropdown;

    public GameObject GameOverContainer;
    public Text GameOverText;
    public Button UndoButton;

    public bool GameOver { get; private set; }

    public static GameMode GameMode { get; private set; }
    public static Turn Turn { get; private set; }

    private Stack<Cell> _moveList;

    protected override void Awake()
    {
        EventAggregator.EventAggregator.Instance.Subscribe(this);
    }

    protected override void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        Init();
        ChangeTurn(-1);
    }

    private void Init()
    {
        Turn = null;
        GameOver = false;
        GameOverContainer.SetActive(false);
        GameMode = (GameMode)GameModeDropdown.value;
        _moveList = new Stack<Cell>();
        EventAggregator.EventAggregator.Instance.Publish(GameMessage.Init);
    }

    public void Handle(CellSelectedMessage message)
    {
        AddToMoveList(message.Cell);

        EventAggregator.EventAggregator.Instance.Publish(GameMessage.UnhiglightCells);
        EventAggregator.EventAggregator.Instance.Publish(new CheckWinnerMessage(message.Cell.Index, message.Cell.Table.Index));

        if (GameOver)
        {
            SetUndoEnabled(false);
            return;
        }
        ChangeTurn(message.Cell.Index);
    }

    private void AddToMoveList(Cell cell)
    {
        if (_moveList == null)
        {
            _moveList = new Stack<Cell>();
        }

        _moveList.Push(cell);
    }

    public void ChangeTurn(int index)
    {
        PlayerType newPlayer;
        if (Turn == null)
        {
            newPlayer = (PlayerType)StartPlayerDropdown.value;
            Turn = new Turn(newPlayer, CellValue.X);
        }
        else
        {
            newPlayer = PlayerType.Player;
            if (GameMode == GameMode.VsAi)
            {
                newPlayer = Turn.Player.Opponent();
            }
            var newMark = Turn.Mark.Opponent();
            Turn = new Turn(newPlayer, newMark);
        }

        SetUndoEnabled(_moveList.Count > 0 && newPlayer == PlayerType.Player);
        EventAggregator.EventAggregator.Instance.Publish(new TurnChangedMessage(Turn, index));
    }

    private void SetUndoEnabled(bool enable)
    {
        UndoButton.interactable = enable;
    }

    public void Undo()
    {
        UndoMove(GameMode == GameMode.TwoPlayers ? 1 : 2);
    }

    private void UndoMove(int moveCount)
    {
        for (var i = 0; i < moveCount; i++)
        {
            var cell = _moveList.Pop();
            cell.SetValue(CellValue.Unset, true);
            cell.Table.CellValue = CellValue.Unset;
        }

        EventAggregator.EventAggregator.Instance.Publish(GameMessage.UnhiglightCells);
        SetUndoEnabled(_moveList.Count != 0);
        var tableIndex = _moveList.Count == 0 ? -1 : _moveList.Peek().Index;
        EventAggregator.EventAggregator.Instance.Publish(new TurnChangedMessage(Turn, tableIndex));
    }

    public void Handle(GameOverMessage message)
    {
        GameOver = true;
        if (message.Winner == CellValue.Draw)
        {
            GameOverText.text = "DRAW";
        }
        else if (message.Winner != CellValue.Unset)
        {
            GameOverText.text = message.Winner + " WON THE GAME!";
        }
        GameOverContainer.SetActive(true);
    }
}