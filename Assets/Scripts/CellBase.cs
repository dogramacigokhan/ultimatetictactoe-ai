﻿using Enums;
using UnityEngine;
using UnityEngine.EventSystems;

public class CellBase : UIBehaviour
{
    public int Index = 0;

    public Table Table;

    public CellValue CellValue = CellValue.Unset;
}