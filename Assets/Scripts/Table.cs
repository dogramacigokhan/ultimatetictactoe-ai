﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Enums;
using EventAggregator;
using EventMessages;
using UnityEngine;

public class Table : CellBase, IHandle<GameMessage>, IHandle<TurnChangedMessage>, IHandle<CheckWinnerMessage>
{
    public List<CellBase> Cells;

    private int _prevCellIndex;
    public int LastCellIndex { get; private set; }

    private int TwoXOnRow;
    private int TwoOOnRow;

    public CellBase this[int i]
    {
        get { return Cells[i]; }
    }

    protected override void Awake()
    {
        base.Awake();
        EventAggregator.EventAggregator.Instance.Subscribe(this);

        TwoXOnRow = 2 * (int)CellValue.X + (int)CellValue.Unset;
        TwoOOnRow = 2 * (int)CellValue.O + (int)CellValue.Unset;
    }

    public void Handle(GameMessage message)
    {
        if (message == GameMessage.Init)
        {
            ClearTable();
        }
        if (message == GameMessage.UnhiglightCells)
        {
            UnhiglightCells();
        }
    }

    public void Handle(CheckWinnerMessage message)
    {
        if (Index != message.TableIndex)
        {
            return;
        }

        if (CellValue != CellValue.Unset)
        {
            return;
        }

        CellValue winner;
        var result = CheckWinner(out winner);
        if (!result)
        {
            return;
        }

        HighlightTable(winner);
        CellValue = winner;

        if (Table != null)
        {
            // Set ultimate table's last index to this table's index
            Table.LastCellIndex = Index;
            // Inform the ultimate table with this table's and ultimate table's index (-1)
            EventAggregator.EventAggregator.Instance.Publish(new CheckWinnerMessage(Index, -1));
        }
        else
        {
            // Ultimate table, game is over
            EventAggregator.EventAggregator.Instance.Publish(new GameOverMessage(winner));
        }
    }

    public List<int> GetAvailableMoves()
    {
        var list = new List<int>();
        for (var i = 0; i < Cells.Count; i++)
        {
            var cell = Cells[i];
            if (cell.CellValue == CellValue.Unset)
            {
                list.Add(i);
            }
        }
        return list;
    }

    private void ClearTable()
    {
        LastCellIndex = 0;
        //CellValue = CellValue.Unset;
        foreach (var cellBase in Cells)
        {
            var cell = cellBase as Cell;
            if (cell != null)
            {
                cell.Clear();
            }
        }
    }

    public void SetCell(int index, CellValue value)
    {
        if (Cells == null || Cells.Count <= index)
        {
            Debug.LogError("Cells are null or empty!", transform);
            return;
        }
        _prevCellIndex = LastCellIndex;
        LastCellIndex = index;
        Cells[index].CellValue = value;
    }

    public void UnsetCell(int index)
    {
        LastCellIndex = _prevCellIndex;
        Cells[index].CellValue = CellValue.Unset;
    }

    public int GetHeuristicTableValue(int tableIndex, CellValue turn, int bonus)
    {
        if (Table != null)
        {
            // Return if this is not the ultimate table
            return 0;
        }

        const int n = 3;

        var pos = new int[4];
        var row = tableIndex / 3;
        var col = tableIndex % 3;
        for (var i = 0; i < n; i++)
        {
            pos[0] += (int)Cells[col + i * n].CellValue; // Column
            pos[1] += (int)Cells[row * n + i].CellValue; // Row
            pos[2] += (int)Cells[i * n + i].CellValue; // Diagonal
            pos[3] += (int)Cells[i * n + n - (i + 1)].CellValue; // R-Diagonal
        }

        foreach (var position in pos)
        {
            if ((turn == CellValue.X && position == TwoXOnRow)
                || (turn == CellValue.O && position == TwoOOnRow))
            {
                return bonus;
            }
        }
        return 0;
    }

    public bool CheckWinner(out CellValue winner)
    {
        const int n = 3;
        winner = CellValue.Unset;

        var pos = new int[4];
        var row = LastCellIndex / 3;
        var col = LastCellIndex % 3;
        for (var i = 0; i < n; i++)
        {
            pos[0] += (int)Cells[col + i * n].CellValue; // Column
            pos[1] += (int)Cells[row * n + i].CellValue; // Row
            pos[2] += (int)Cells[i * n + i].CellValue; // Diagonal
            pos[3] += (int)Cells[i * n + n - (i + 1)].CellValue; // R-Diagonal
        }
        var maybeDraw = true;
        foreach (var position in pos)
        {
            if (position == n)
            {
                winner = CellValue.X;
                return true;
            }
            if (position == -n)
            {
                winner = CellValue.O;
                return true;
            }
            if (position >= (int)CellValue.Unset)
            {
                // There is an empty cell
                maybeDraw = false;
            }
        }

        if (!maybeDraw)
        {
            return false;
        }

        foreach (var cell in Cells)
        {
            if (cell.CellValue == CellValue.Unset)
            {
                return false;
            }
        }
        winner = CellValue.Draw;
        return true;
    }

    public void Handle(TurnChangedMessage message)
    {
        HiglightSelectableCells(message.Turn.Mark, message.TableIndex);
    }

    private void HighlightTable(CellValue winner)
    {
        if (Table == null || winner == CellValue.Unset)
        {
            // Skip ultimate table
            return;
        }

        foreach (var cellBase in Cells)
        {
            var cell = cellBase as Cell;
            if (cell != null)
            {
                cell.Highlight(winner, true);
            }
        }
    }

    private void HiglightSelectableCells(CellValue mark, int tableIndex)
    {
        if (Table == null)
        {
            // Skip ultimate table
            return;
        }
        if (Index != tableIndex && tableIndex != -1)
        {
            // Skip if index does not match
            return;
        }
        if (Index == tableIndex && CellValue != CellValue.Unset)
        {
            // Trying to highlight already won table, highlight every available move
            EventAggregator.EventAggregator.Instance.Publish(new TurnChangedMessage(GameManager.Turn, -1));
        }

        foreach (var cellBase in Cells)
        {
            var cell = cellBase as Cell;
            if (cell != null)
            {
                cell.Highlight(mark);
            }
        }
    }

    public void UnhiglightCells()
    {
        if (CellValue != CellValue.Unset)
        {
            // A player already won this table, don't touch it
            return;
        }

        foreach (var cellBase in Cells)
        {
            var cell = cellBase as Cell;
            if (cell != null)
            {
                if (cell.CellValue == CellValue.Unset)
                {
                    cell.UnHighlight();
                }
            }
        }
    }

    public bool Empty()
    {
        return Cells.All(cell => cell.CellValue == CellValue.Unset);
    }

    public void Print()
    {
        var str = new StringBuilder();
        var row = string.Empty;
        for (var i = 0; i < Cells.Count; i++)
        {
            var cell = Cells[i];
            var separator = (i % 3 != 2) ? "|" : "";
            var value = cell.CellValue == CellValue.Unset ? "E" : cell.CellValue.ToString();
            row += value + separator;

            if (i > 0 && (i + 1) % 3 == 0)
            {
                str.AppendLine(row);
                row = string.Empty;
                if (i != Cells.Count) str.AppendLine("-------");
            }
        }
        Debug.Log(str.ToString());
    }
}