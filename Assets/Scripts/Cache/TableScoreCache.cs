﻿using System.Collections.Generic;
using Enums;

namespace Cache
{
    public class TableScoreCache
    {
        private Dictionary<TableScoreKey, int> _cache = new Dictionary<TableScoreKey, int>();

        public bool Get(Table table, CellValue mark, out TableScoreKey key, out int value)
        {
            key = TableScoreKey.ConstructKey(table, mark);
            return _cache.TryGetValue(key, out value);
        }

        public void Add(TableScoreKey key, int value)
        {
            _cache[key] = value;
        }

        public void Initialize()
        {
            _cache = new Dictionary<TableScoreKey, int>();
        }

        public int Count()
        {
            return _cache.Count;
        }
    }
}