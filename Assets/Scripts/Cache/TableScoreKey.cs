﻿using Enums;

namespace Cache
{
    public class TableScoreKey
    {
        protected bool Equals(TableScoreKey other)
        {
            return Key == other.Key;
        }

        public uint Key { get; private set; }

        public TableScoreKey(uint key)
        {
            Key = key;
        }

        public static TableScoreKey ConstructKey(Table table, CellValue mark)
        {
            uint key = 0;
            if (mark == CellValue.X)
            {
                key |= 1 << 27;
            }

            for (var i = 0; i < table.Cells.Count; i++)
            {
                var cell = table.Cells[i];
                var shiftValue = ShiftValue(cell.CellValue) - i;
                key |= (uint)(1 << shiftValue);
            }
            return new TableScoreKey(key);
        }

        private static int ShiftValue(CellValue value)
        {
            switch (value)
            {
                case CellValue.Unset:
                    return 26;
                case CellValue.X:
                    return 17;
                case CellValue.O:
                    return 8;
                default:
                    return 0;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableScoreKey) obj);
        }

        public override int GetHashCode()
        {
            return (int) Key;
        }
    }
}