﻿namespace Enums
{
    public static class EnumExtension
    {
        public static CellValue Opponent(this CellValue value)
        {
            if (value == CellValue.Draw || value == CellValue.Unset)
            {
                return value;
            }
            return value == CellValue.X ? CellValue.O : CellValue.X;
        }

        public static PlayerType Opponent(this PlayerType player)
        {
            return player == PlayerType.Player ? PlayerType.Ai : PlayerType.Player;
        }
    }
}