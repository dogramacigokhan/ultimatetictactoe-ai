﻿namespace Enums
{
    public enum CellValue
    {
        O = -1,
        X = 1,
        Draw = 100,
        Unset = 1000
    }
}