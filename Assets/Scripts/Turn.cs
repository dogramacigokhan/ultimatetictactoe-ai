﻿using Enums;

public class Turn
{
    public PlayerType Player { get; private set; }
    public CellValue Mark { get; private set; }

    public Turn(PlayerType player, CellValue mark)
    {
        Player = player;
        Mark = mark;
    }
}