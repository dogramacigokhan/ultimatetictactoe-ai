﻿namespace EventMessages
{
    public class CheckWinnerMessage
    {
        public int LastMarkIndex { get; private set; }
        public int TableIndex { get; private set; }

        public CheckWinnerMessage(int lastMarkIndex, int tableIndex)
        {
            LastMarkIndex = lastMarkIndex;
            TableIndex = tableIndex;
        }
    }
}