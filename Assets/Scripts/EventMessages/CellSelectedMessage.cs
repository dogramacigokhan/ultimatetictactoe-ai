﻿namespace EventMessages
{
    public class CellSelectedMessage
    {
        public Cell Cell { get; private set; }

        public CellSelectedMessage(Cell cell)
        {
            Cell = cell;
        }
    }
}
