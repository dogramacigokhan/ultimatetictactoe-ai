﻿using Enums;

namespace EventMessages
{
    public class GameOverMessage
    {
        public CellValue Winner { get; private set; }

        public GameOverMessage(CellValue winner)
        {
            Winner = winner;
        }
    }
}