﻿namespace EventMessages
{
    public class TurnChangedMessage
    {
        public Turn Turn { get; private set; }

        public int TableIndex { get; private set; }

        public TurnChangedMessage(Turn turn, int tableIndex)
        {
            Turn = turn;
            TableIndex = tableIndex;
        }
    }
}
