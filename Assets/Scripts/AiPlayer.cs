﻿using System.Collections;
using System.Linq;
using Cache;
using Enums;
using EventAggregator;
using EventMessages;
using UnityEngine;
using UnityEngine.EventSystems;

public class AiPlayer : UIBehaviour, IHandle<TurnChangedMessage>, IHandle<GameMessage>
{
    public Table UltimateTable;
    public CellValue AiMark { get; private set; }
    public int NextTableEvalDepth;

    public int ToWonTablePenalty;
    public int ToMidTablePenalty;
    public int ToEmptyTableBonus;
    public int ToPlayThirdOnRowBonus;
    public int GameWonBonus;

    public float MoveDurationInSeconds;

    private int _nextTableIndex;

    private int _evalCounter;
    private bool _stopMovement;
    private bool _movementCompleted;

    private int _moveIndex;
    private TableScoreCache _cache;

    protected override void Awake()
    {
        EventAggregator.EventAggregator.Instance.Subscribe(this);
    }

    public void Handle(GameMessage message)
    {
        if (message == GameMessage.Init)
        {
            _cache = new TableScoreCache();
            _cache.Initialize();
        }
    }

    public void Handle(TurnChangedMessage message)
    {
        if (message.Turn.Player != PlayerType.Ai)
        {
            return;
        }

        if (message.TableIndex != -1)
        {
            var table = UltimateTable[message.TableIndex] as Table;
            if (table.CellValue != CellValue.Unset)
            {
                // Another message will be sent with -1 table index
                return;
            }
        }

        var tableIndex = SelectTableIndex(message.TableIndex, message.Turn.Mark);
        StartCoroutine(CheckDuration(tableIndex, message.Turn.Mark));
        StartCoroutine(MoveCoro(tableIndex, message.Turn.Mark));
    }

    private int SelectTableIndex(int tableIndex, CellValue mark)
    {
        if (tableIndex >= 0 && UltimateTable[tableIndex].CellValue == CellValue.Unset)
        {
            return tableIndex;
        }

        var bestIndex = tableIndex;
        var bestScore = 0;
        for (var i = 0; i < 9; i++)
        {
            var table = UltimateTable[i] as Table;
            if (bestIndex == -1 && table.CellValue == CellValue.Unset)
            {
                bestIndex = i;
            }

            if (table.CellValue != CellValue.Unset)
            {
                // Cannot move to alread-won table
                continue;
            }

            TableScoreKey key;
            int score;
            if (!_cache.Get(table, mark, out key, out score))
            {
                ExecuteAlphaBeta(table, mark, NextTableEvalDepth, out score);
                _cache.Add(key, score);
            }
            if (score > bestScore)
            {
                bestScore = score;
                bestIndex = i;
            }
        }
        Debug.Log("Selected index: " + bestIndex);
        return bestIndex;
    }

    private IEnumerator MoveCoro(int tableIndex, CellValue mark)
    {
        yield return null;
        _moveIndex = Move(tableIndex, mark);
    }

    private int Move(int tableIndex, CellValue mark)
    {
        AiMark = mark;
        _movementCompleted = false;
        _nextTableIndex = 0;

        var timer = System.Diagnostics.Stopwatch.StartNew();
        _evalCounter = 0;

        int score;
        var bestMove = ExecuteAlphaBeta(tableIndex, mark, NextTableEvalDepth, out score);

        _movementCompleted = true;
        _stopMovement = false;
        timer.Stop();

        Debug.LogFormat("A/B Search Score: {0} BestMove: {1} Duration(ms): {2} Eval Count: {3} Cache Count: {4}", score,
            bestMove, timer.ElapsedMilliseconds, _evalCounter, _cache.Count());
        return bestMove;
    }

    private IEnumerator CheckDuration(int tableIndex, CellValue turnMark)
    {
        var duration = MoveDurationInSeconds * 1000;
        var timer = System.Diagnostics.Stopwatch.StartNew();

        while (!_movementCompleted && timer.ElapsedMilliseconds < duration)
        {
            yield return new WaitForSeconds(0.1f);
        }

        if (_movementCompleted && timer.ElapsedMilliseconds < duration)
        {
            yield return new WaitForSeconds(MoveDurationInSeconds - (float)timer.ElapsedMilliseconds / 1000);
        }
        else if (!_movementCompleted)
        {
            _stopMovement = true;
            while (!_movementCompleted)
            {
                yield return null;
            }
            Debug.LogWarning("Aborted AI move coroutine after " + timer.ElapsedMilliseconds + " milliseconds");
        }

        timer.Stop();
        EndMovement(tableIndex, _moveIndex, turnMark);
    }

    private void EndMovement(int tableIndex, int moveIndex, CellValue mark)
    {
        var table = UltimateTable[tableIndex] as Table;
        if (table == null)
        {
            Debug.LogError("Unable to find table with index: " + tableIndex);
            return;
        }

        ((Cell)table.Cells[moveIndex]).SetValue(mark);
    }

    private int ExecuteAlphaBeta(int tableIndex, CellValue mark, int nextTableEvalDepth, out int score)
    {
        var table = UltimateTable[tableIndex] as Table;
        return ExecuteAlphaBeta(table, mark, nextTableEvalDepth, out score);
    }

    private int ExecuteAlphaBeta(Table table, CellValue mark, int nextTableEvalDepth, out int score)
    {
        var depth = table.Cells.Count(c => c.CellValue == CellValue.Unset);
        return AlphaBeta(table, depth, depth, -1000, 1000, mark, mark, nextTableEvalDepth, out score);
    }

    private int AlphaBeta(Table table, int startDepth, int depth, int alpha, int beta, CellValue startTurn,
        CellValue turn, int nextTableEvalDepth, out int score)
    {
        score = 0;
        var bestMove = 0;

        if (_stopMovement)
        {
            return 0;
        }

        CellValue winner;
        var tableWon = table.CheckWinner(out winner);
        if (depth == 0 || tableWon)
        {
            score = Eval(table, startTurn, turn, tableWon, winner, depth);
            return bestMove;
        }

        var best = -1000;
        var availableMoves = table.GetAvailableMoves();
        foreach (var moveIndex in availableMoves)
        {
            if (_stopMovement)
            {
                break;
            }

            if (startDepth == depth)
            {
                _nextTableIndex = moveIndex;
            }

            table.SetCell(moveIndex, turn);

            int childScore;
            TableScoreKey cacheKey;
            int cacheResult;

            var cacheExists = _cache.Get(table, turn.Opponent(), out cacheKey, out cacheResult);
            if (cacheExists)
            {
                childScore = cacheResult;
            }
            else
            {
                AlphaBeta(table, depth, depth - 1, -beta, -alpha, startTurn, turn.Opponent(), nextTableEvalDepth,
                    out childScore);
                _cache.Add(cacheKey, childScore);
            }
            childScore *= -1;

            if (startDepth == depth)
            {
                childScore += EvalNextTable(_nextTableIndex, turn.Opponent(), nextTableEvalDepth);
                //Debug.LogFormat("MoveIndex: {0} Result: {1}", moveIndex, childScore);
            }

            alpha = Mathf.Max(childScore, alpha);
            if (childScore > best)
            {
                best = childScore;
                bestMove = moveIndex;
            }

            table.UnsetCell(moveIndex);
            if (alpha >= beta)
            {
                // Alpha-beta cutoff
                break;
            }
        }
        score = best;
        return bestMove;
    }

    private int Eval(Table table, CellValue startTurn, CellValue turn, bool tableWon, CellValue winner, int depth)
    {
        var score = 0;
        var modifier = (turn == startTurn ? 1 : -1);

        if (!tableWon && winner == CellValue.Unset)
        {
            tableWon = table.CheckWinner(out winner);
        }

        CellValue ultimateWinner;
        var ultimateTableWon = UltimateTable.CheckWinner(out ultimateWinner);
        if (ultimateTableWon)
        {
            score += GameWonBonus * modifier;
        }

        if (!tableWon || winner == CellValue.Draw)
        {
            return score;
        }

        var tableScore = (10 + depth) * (winner == startTurn ? 1 : -1);
        score += tableScore * modifier;
        _evalCounter++;
        return score;
    }

    private int EvalNextTable(int tableIndex, CellValue mark, int depth)
    {
        var evalScore = 0;
        if (depth == 0)
        {
            return evalScore;
        }

        if (tableIndex == 4)
        {
            evalScore += ToMidTablePenalty;
        }

        var table = UltimateTable[tableIndex] as Table;
        if (table.Empty())
        {
            evalScore -= ToEmptyTableBonus;
        }

        evalScore += UltimateTable.GetHeuristicTableValue(tableIndex, mark, ToPlayThirdOnRowBonus);

        if (table.CellValue != CellValue.Unset)
        {
            // TODO: Evaluate next best table
            // Penalty points to the move if it redirects opponent to a won table
            evalScore += ToWonTablePenalty;
            return -evalScore;
        }

        int score;
        ExecuteAlphaBeta(table, mark, depth - 1, out score);
        evalScore += score;
        evalScore += depth;

        return -evalScore;
    }
}