#### Ultimate Tic Tac Toe

AI implementation for Ultimate Tic Tac Toe game. An example game can be found in [here](http://theaigames.com/competitions/ultimate-tic-tac-toe).

![ultimate-tic-tac-toe](http://theaigames.com/img/tictactoe-rules2.jpg)

#### Building the Source Code
* Unity 5.6+ is required.